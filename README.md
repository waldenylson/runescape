# RuneScape 718/830
Código do servidor de RuneScape 718/830 baseado no **[Valkyr](https://forum.runelocus.com/topic/102825-718830-valkyr-probably-the-most-stable-718830-server-released/)**, portado para tecnologias mais novas. O código está sendo refatorado em Java 8 para ficar mais flexível e suportar maiores alterações.

## Tecnologias utilizadas
* Java 8
* Maven
* Project Lombok
* Netty
* jUnit

## IDEs utilizadas
* VSCode
* Eclipse
* STS