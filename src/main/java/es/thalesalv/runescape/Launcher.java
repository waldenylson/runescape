package es.thalesalv.runescape;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.alex.store.Index;

import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.cache.loaders.ItemDefinitions;
import es.thalesalv.runescape.cache.loaders.NPCDefinitions;
import es.thalesalv.runescape.cache.loaders.ObjectDefinitions;
import es.thalesalv.runescape.cores.CoresManager;
import es.thalesalv.runescape.exception.InitializationError;
import es.thalesalv.runescape.exception.SaveError;
import es.thalesalv.runescape.game.GameEngine;
import es.thalesalv.runescape.game.RegionBuilder;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.npc.combat.CombatScriptsHandler;
import es.thalesalv.runescape.game.player.LendingManager;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.FishingSpotsHandler;
import es.thalesalv.runescape.game.player.content.FriendChatsManager;
import es.thalesalv.runescape.game.player.content.botanybay.BotanyBay;
import es.thalesalv.runescape.game.player.content.clans.ClansManager;
import es.thalesalv.runescape.game.player.content.grandExchange.GrandExchange;
import es.thalesalv.runescape.game.player.controlers.ControlerHandler;
import es.thalesalv.runescape.game.player.cutscenes.CutscenesHandler;
import es.thalesalv.runescape.game.player.dialogues.DialogueHandler;
import es.thalesalv.runescape.game.worldlist.WorldList;
import es.thalesalv.runescape.mvh.AuthService;
import es.thalesalv.runescape.mvh.Motivote;
import es.thalesalv.runescape.net.ServerChannelHandler;
import es.thalesalv.runescape.utils.Censor;
import es.thalesalv.runescape.utils.DTRank;
import es.thalesalv.runescape.utils.DisplayNames;
import es.thalesalv.runescape.utils.IPBanL;
import es.thalesalv.runescape.utils.ItemBonuses;
import es.thalesalv.runescape.utils.ItemExamines;
import es.thalesalv.runescape.utils.ItemSpawns;
import es.thalesalv.runescape.utils.KillStreakRank;
import es.thalesalv.runescape.utils.Logger;
import es.thalesalv.runescape.utils.MapArchiveKeys;
import es.thalesalv.runescape.utils.MapAreas;
import es.thalesalv.runescape.utils.MusicHints;
import es.thalesalv.runescape.utils.NPCBonuses;
import es.thalesalv.runescape.utils.NPCCombatDefinitionsL;
import es.thalesalv.runescape.utils.NPCDrops;
import es.thalesalv.runescape.utils.NPCExamines;
import es.thalesalv.runescape.utils.NPCSpawning;
import es.thalesalv.runescape.utils.NPCSpawns;
import es.thalesalv.runescape.utils.ObjectSpawning;
import es.thalesalv.runescape.utils.ObjectSpawns;
import es.thalesalv.runescape.utils.PkRank;
import es.thalesalv.runescape.utils.SerializableFilesManager;
import es.thalesalv.runescape.utils.ShopsHandler;
import es.thalesalv.runescape.utils.Utils;
import es.thalesalv.runescape.utils.huffman.Huffman;

public final class Launcher {

	public static void main(String[] args) throws Exception {
		long currentTime = Utils.currentTimeMillis();
		Logger.log("Vuse-RSPS", "Reading Cache Intake...");
		Cache.init();
		AuthService.setProvider(new Motivote("njNfBGvYXZvJunDQYFBuhHOiHoDMhQEL"));
		// Logger.log("Vuse-RSPS", "Preparing MYSQL Database...");
		// World.database().connect();
		KillStreakRank.init();
		GrandExchange.init();
		ItemSpawns.init();
		Huffman.init();
		Logger.log("Vuse-RSPS", "Loading Data...");
		World.loadWell();
		WorldList.init();
		Censor.init();
		DisplayNames.init();
		IPBanL.init();
		PkRank.init();
		DTRank.init();
		MapArchiveKeys.init();
		MapAreas.init();
		ObjectSpawns.init();
		NPCSpawns.init();
		NPCCombatDefinitionsL.init();
		NPCBonuses.init();
		NPCDrops.init();
		ItemExamines.init();
		ItemBonuses.init();
		MusicHints.init();
		BotanyBay.init();
		ShopsHandler.init();
		NPCExamines.init();
		Logger.log("Vuse-RSPS", "Loading Global Spawns...");
		NPCSpawning.removeObjects();
		ObjectSpawning.spawnNPCS();
		FishingSpotsHandler.init();
		Logger.log("Vuse-RSPS", "Loading Combat Scripts...");
		CombatScriptsHandler.init();
		Logger.log("Launcher", "Initiating Clans Manager...");
		ClansManager.init();
		Logger.log("Launcher", "Initiating Lent Items...");
		LendingManager.init();
		// Logger.log("Vuse-RSPS", "Reading Local Handlers...");
		// Logger.log("Vuse-RSPS", "Reading Local Controlers...");
		// Logger.log("Vuse-RSPS", "Reading Local Managers...");
		/*
		 * Game Engine
		 */
		Logger.log("Vuse-RSPS", "Preparing Game Engine...");
		GameEngine.get().init();
		/*
		 * Grand Exchange
		 */
		// Logger.log("Vuse-RSPS", "Preparing Grand Exchange...");
		// tradeAbleItems.initialize();
		// GrandExchangePriceLoader.initialize();
		// Offers.load();
		DialogueHandler.init();
		ControlerHandler.init();
		CutscenesHandler.init();
		FriendChatsManager.init();
		Startup(); // Extends cpanel jframe
		Logger.log("Launcher", "Initiating Control Panel...");
		CoresManager.init();
		Logger.log("Vuse-RSPS", "Loading World...");
		World.init();
		Logger.log("Vuse-RSPS", "Loading Region Builder...");
		RegionBuilder.init();
		try {
			ServerChannelHandler.init();
		} catch (Throwable e) {
			throw new InitializationError(e.getMessage(), e);
		}
		Logger.log(Settings.SERVER_NAME,
				"Server took " + (Utils.currentTimeMillis() - currentTime) + " milliseconds to launch.");
		addAccountsSavingTask();
		if (Settings.HOSTED)
			addCleanMemoryTask();
		// Donations.init();
		// HalloweenEvent.startEvent();
		addrecalcPricesTask();
		// World.spawnObject(new WorldObject(87309, 10, 3122, 3225, 0, 0));
		Logger.log("World", "Vuse-RSPS is now Online!");
	}

	private static void addrecalcPricesTask() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		int minutes = (int) ((c.getTimeInMillis() - Utils.currentTimeMillis()) / 1000 / 60);
		int halfDay = 12 * 60;
		if (minutes > halfDay)
			minutes -= halfDay;
		CoresManager.slowExecutor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				try {
					GrandExchange.recalcPrices();
				} catch (Throwable e) {
					Logger.handle(e);
				}

			}
		}, minutes, halfDay, TimeUnit.MINUTES);
	}

	private static void addCleanMemoryTask() {
		CoresManager.slowExecutor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				try {
					cleanMemory(Runtime.getRuntime().freeMemory() < Settings.MIN_FREE_MEM_ALLOWED);
				} catch (Throwable e) {
					Logger.handle(e);
				}
			}
		}, 0, 10, TimeUnit.MINUTES);
	}

	public static void Startup() {
		Gui.main(null);
	}

	private static void addAccountsSavingTask() {
		CoresManager.slowExecutor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				try {
					saveFiles();
					Logger.log("Online", "There are currently " + (World.getPlayers().size()) + " players playing "
							+ Settings.SERVER_NAME + ".");
				} catch (Throwable e) {
					Logger.handle(e);
				}

			}
		}, 1, 1, TimeUnit.MINUTES);// can be changed to seconds using "TimeUnit.SECONDS" as of now every one minute
									// it will save the players.
	}

	public static void saveFiles() throws SaveError {
		try {
			for (Player player : World.getPlayers()) {
				if (player == null || !player.hasStarted() || player.hasFinished())
					continue;
				Logger.log("Game", "Saving character...");
				SerializableFilesManager.savePlayer(player);
			}
			DisplayNames.save();
			IPBanL.save();
			PkRank.save();
			DTRank.save();
			KillStreakRank.save();
			GrandExchange.save();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static void cleanMemory(boolean force) {
		if (force) {
			ItemDefinitions.clearItemsDefinitions();
			NPCDefinitions.clearNPCDefinitions();
			ObjectDefinitions.clearObjectDefinitions();
			/*
			 * for (Region region : World.getRegions().values()) {
			 * region.removeMapFromMemory(); }
			 */
		}
		for (Index index : Cache.STORE.getIndexes())
			index.resetCachedFiles();
		CoresManager.fastExecutor.purge();
		System.gc();
	}

	public static void shutdown() throws InitializationError {
		try {
			GameEngine.get().shutdown();
			closeServices();
		} finally {
			System.exit(0);
		}
	}

	public static void closeServices() throws InitializationError {
		ServerChannelHandler.shutdown();
		CoresManager.shutdown();
		if (Settings.HOSTED) {
			try {
			} catch (Throwable e) {
				throw new InitializationError(e.getMessage(), e);
			}
		}
	}

	public static void restart() throws InitializationError {
		closeServices();
		System.gc();
		try {
			Runtime.getRuntime().exec("java -d64 -Xss50m -cp bin;library/*; Launcher");
			System.exit(0);
		} catch (Throwable e) {
			throw new InitializationError(e.getMessage(), e);
		}

	}

	private Launcher() {

	}

}
