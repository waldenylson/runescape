package es.thalesalv.runescape.exception;

public class DatabaseConnectionError extends Exception {

	private static final long serialVersionUID = 1L;

	public DatabaseConnectionError() {
		super();
	}

	public DatabaseConnectionError(String message) {
		super(message);
	}

	public DatabaseConnectionError(Throwable cause) {
		super(cause);
	}

	public DatabaseConnectionError(String message, Throwable cause) {
		super(message, cause);
	}
}
