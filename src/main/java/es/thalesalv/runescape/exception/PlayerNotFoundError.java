package es.thalesalv.runescape.exception;

public class PlayerNotFoundError extends Throwable {

	private static final long serialVersionUID = 1L;

	public PlayerNotFoundError() {
		super();
	}

	public PlayerNotFoundError(String message) {
		super(message);
	}

	public PlayerNotFoundError(Throwable cause) {
		super(cause);
	}

	public PlayerNotFoundError(String message, Throwable cause) {
		super(message, cause);
	}
}
