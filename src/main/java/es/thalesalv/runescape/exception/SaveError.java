package es.thalesalv.runescape.exception;

public class SaveError extends Exception {
	private static final long serialVersionUID = 1L;

	public SaveError() {
		super();
	}

	public SaveError(String message) {
		super(message);
	}

	public SaveError(Throwable cause) {
		super(cause);
	}

	public SaveError(String message, Throwable cause) {
		super(message, cause);
	}
}
