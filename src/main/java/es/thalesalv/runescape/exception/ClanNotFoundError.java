package es.thalesalv.runescape.exception;

public class ClanNotFoundError extends Throwable {

	private static final long serialVersionUID = 1L;

	public ClanNotFoundError() {
		super();
	}

	public ClanNotFoundError(String message) {
		super(message);
	}

	public ClanNotFoundError(Throwable cause) {
		super(cause);
	}

	public ClanNotFoundError(String message, Throwable cause) {
		super(message, cause);
	}
}