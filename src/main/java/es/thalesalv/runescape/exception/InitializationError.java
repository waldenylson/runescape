package es.thalesalv.runescape.exception;

public class InitializationError extends Exception {

	private static final long serialVersionUID = -1L;

	public InitializationError() {
		super();
	}

	public InitializationError(String message) {
		super("Error initiating server: " + message);
	}

	public InitializationError(Throwable cause) {
		super(cause);
	}

	public InitializationError(String message, Throwable cause) {
		super("Error initiating server: " + message, cause);
	}
}
