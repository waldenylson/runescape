package es.thalesalv.runescape.exception;

public class PackUnpackError extends Exception {
	
	private static final long serialVersionUID = 1L;

	public PackUnpackError() {
		super();
	}

	public PackUnpackError(String message) {
		super(message);
	}

	public PackUnpackError(Throwable cause) {
		super(cause);
	}

	public PackUnpackError(String message, Throwable cause) {
		super(message, cause);
	}
}
