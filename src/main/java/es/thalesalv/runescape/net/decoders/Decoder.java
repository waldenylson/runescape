package es.thalesalv.runescape.net.decoders;

import es.thalesalv.runescape.io.InputStream;
import es.thalesalv.runescape.net.Session;

public abstract class Decoder {

	protected Session session;

	public Decoder(Session session) {
		this.session = session;
	}

	public abstract void decode(InputStream stream);

}
