package es.thalesalv.runescape.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.List;

import es.thalesalv.runescape.exception.InitializationError;
import es.thalesalv.runescape.exception.PackUnpackError;

public class Censor {

	private final static List<String> censoredWords = new ArrayList<String>();
	private final static String PACKED_PATH = "data/packedCensoredWords.e";
	private final static String UNPACKED_PATH = "data/unpackedCensoredWords.txt";

	public static final void init() throws InitializationError {
		try {
			if (new File(PACKED_PATH).exists()) {
				loadPackedCensoredWords();
			} else if (new File(UNPACKED_PATH).exists()) {
				loadUnpackedCensoredWords();
			}
		} catch (Exception e) {
			throw new InitializationError(e.getMessage(), e);
		}
	}

	public static String getFilteredMessage(String message) {
		message = message.toLowerCase();
		for (String word : censoredWords) {
			if (message.contains(word)) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < word.length(); i++)
					sb.append("*");
				message = message.replace(word, sb.toString());
			}
		}
		return Utils.fixChatMessage(message);
	}

	private static void loadPackedCensoredWords() throws PackUnpackError {
		try {
			RandomAccessFile in = new RandomAccessFile(PACKED_PATH, "r");
			FileChannel channel = in.getChannel();
			ByteBuffer buffer = channel.map(MapMode.READ_ONLY, 0, channel.size());
			while (buffer.hasRemaining())
				censoredWords.add(readAlexString(buffer));
			channel.close();
			in.close();
		} catch (Throwable e) {
			throw new PackUnpackError(e.getMessage(), e);
		}
	}

	private static void loadUnpackedCensoredWords() throws PackUnpackError {
		Logger.log("Censor", "Packing censored words...");
		try {
			BufferedReader in = new BufferedReader(new FileReader(UNPACKED_PATH));
			DataOutputStream out = new DataOutputStream(new FileOutputStream(PACKED_PATH));
			while (true) {
				String line = in.readLine();
				if (line == null)
					break;
				if (line.startsWith("//") || line.startsWith("*"))
					continue;
				writeAlexString(out, line);
				censoredWords.add(line);
			}

			in.close();
			out.flush();
			out.close();
		} catch (Exception e) {
			throw new PackUnpackError(e.getMessage(), e);
		}

	}

	public static String readAlexString(ByteBuffer buffer) {
		int count = buffer.get() & 0xff;
		byte[] bytes = new byte[count];
		buffer.get(bytes, 0, count);
		return new String(bytes);
	}

	public static void writeAlexString(DataOutputStream out, String string) throws IOException {
		byte[] bytes = string.getBytes();
		out.writeByte(bytes.length);
		out.write(bytes);
	}
}
