package es.thalesalv.runescape.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.HashMap;

import es.thalesalv.runescape.exception.InitializationError;
import es.thalesalv.runescape.exception.PackUnpackError;

public final class NPCBonuses {
	private final static HashMap<Integer, int[]> npcBonuses = new HashMap<Integer, int[]>();
	private static final String PACKED_PATH = "data/npcs/packedBonuses.nb";

	public static void init() throws InitializationError {

		try {
			if (new File(PACKED_PATH).exists()) {
				loadPackedNPCBonuses();
			} else {
				loadUnpackedNPCBonuses();
			}
		} catch (Exception e) {
			throw new InitializationError(e.getMessage(), e);
		}
	}

	public static int[] getBonuses(int id) {
		return npcBonuses.get(id);
	}

	@SuppressWarnings("resource")
	private static void loadUnpackedNPCBonuses() throws PackUnpackError {
		Logger.log("NPCBonuses", "Packing npc bonuses...");
		try {
			DataOutputStream out = new DataOutputStream(new FileOutputStream(PACKED_PATH));
			BufferedReader in = new BufferedReader(new FileReader("data/npcs/unpackedBonuses.txt"));
			while (true) {
				String line = in.readLine();
				if (line == null)
					break;
				if (line.startsWith("//"))
					continue;
				String[] splitedLine = line.split(" - ", 2);
				if (splitedLine.length != 2)
					throw new RuntimeException("Invalid NPC Bonuses line: " + line);
				int npcId = Integer.parseInt(splitedLine[0]);
				String[] splitedLine2 = splitedLine[1].split(" ", 10);
				if (splitedLine2.length != 10)
					throw new RuntimeException("Invalid NPC Bonuses line: " + line);
				int[] bonuses = new int[10];
				out.writeShort(npcId);
				for (int i = 0; i < bonuses.length; i++) {
					bonuses[i] = Integer.parseInt(splitedLine2[i]);
					out.writeShort(bonuses[i]);
				}
				npcBonuses.put(npcId, bonuses);
			}
			in.close();
			out.close();
		} catch (Throwable e) {
			throw new PackUnpackError(e.getMessage(), e);
		}
	}

	private static void loadPackedNPCBonuses() throws PackUnpackError {
		try {
			RandomAccessFile in = new RandomAccessFile(PACKED_PATH, "r");
			FileChannel channel = in.getChannel();
			ByteBuffer buffer = channel.map(MapMode.READ_ONLY, 0, channel.size());
			while (buffer.hasRemaining()) {
				int npcId = buffer.getShort() & 0xffff;
				int[] bonuses = new int[10];
				for (int i = 0; i < bonuses.length; i++)
					bonuses[i] = buffer.getShort();
				npcBonuses.put(npcId, bonuses);
			}
			channel.close();
			in.close();
		} catch (Exception e) {
			throw new PackUnpackError(e.getMessage(), e);
		}
	}

	private NPCBonuses() {

	}
}
