// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ShopsHandler.java

package es.thalesalv.runescape.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Iterator;

import es.thalesalv.runescape.exception.InitializationError;
import es.thalesalv.runescape.exception.PackUnpackError;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.Shop;

// Referenced classes of package es.thalesalv.runescape.utils:
//            Logger

public class ShopsHandler {

	public ShopsHandler() {
	}

	public static void init() throws InitializationError {
		try {
			File file = new File("data/items/packedShops.s");
			if (file.exists()) {
				file.delete();
			}

			loadUnpackedShops();
			loadPackedShops();
		} catch (PackUnpackError e) {
			throw new InitializationError(e.getMessage(), e);
		}
	}

	@SuppressWarnings("resource")
	public static void loadUnpackedShops() throws PackUnpackError {
		Logger.log("ShopsHandler", "Packing shops...");
		try {
			BufferedReader in = new BufferedReader(new FileReader(UNPACKED_PATH));
			DataOutputStream out = new DataOutputStream(new FileOutputStream(PACKED_PATH));
			while (true) {
				String line = in.readLine();
				if (line == null)
					break;
				if (line.startsWith("//"))
					continue;
				String[] splitedLine = line.split(" - ", 3);
				if (splitedLine.length != 3)
					throw new RuntimeException("Invalid list for shop line: " + line);
				String[] splitedInform = splitedLine[0].split(" ", 3);
				if (splitedInform.length != 3)
					throw new RuntimeException("Invalid list for shop line: " + line);
				String[] splitedItems = splitedLine[2].split(" ");
				int key = Integer.valueOf(splitedInform[0]);
				int money = Integer.valueOf(splitedInform[1]);
				boolean generalStore = Boolean.valueOf(splitedInform[2]);
				Item[] items = new Item[splitedItems.length / 2];
				int count = 0;
				for (int i = 0; i < items.length; i++)
					items[i] = new Item(Integer.valueOf(splitedItems[count++]), Integer.valueOf(splitedItems[count++]),
							true);
				out.writeInt(key);
				writeAlexString(out, splitedLine[1]);
				out.writeShort(money);
				out.writeBoolean(generalStore);
				out.writeByte(items.length);
				for (Item item : items) {
					out.writeShort(item.getId());
					out.writeInt(item.getAmount());
				}
				addShop(key, new Shop(splitedLine[1], money, items, generalStore));
			}
			in.close();
			out.close();
		} catch (Throwable e) {
			throw new PackUnpackError(e.getMessage(), e);
		}
	}

	private static void loadPackedShops() throws PackUnpackError {
		try {
			RandomAccessFile in = new RandomAccessFile("data/items/packedShops.s", "r");
			FileChannel channel = in.getChannel();
			int key;
			String name;
			int money;
			boolean generalStore;
			Item items[];
			for (ByteBuffer buffer = channel.map(java.nio.channels.FileChannel.MapMode.READ_ONLY, 0L,
					channel.size()); buffer.hasRemaining(); addShop(key, new Shop(name, money, items, generalStore))) {
				key = buffer.getInt();
				name = readAlexString(buffer);
				money = buffer.getShort() & 0xffff;
				generalStore = buffer.get() == 1;
				items = new Item[buffer.get() & 0xff];
				for (int i = 0; i < items.length; i++) {
					items[i] = new Item(buffer.getShort() & 0xffff, buffer.getInt());
				}
			}

			channel.close();
			in.close();
		} catch (Exception e) {
			throw new PackUnpackError(e.getMessage(), e);
		}
	}

	public static String readAlexString(ByteBuffer buffer) {
		int count = buffer.get() & 0xfff;
		byte bytes[] = new byte[count];
		buffer.get(bytes, 0, count);
		return new String(bytes);
	}

	public static void writeAlexString(DataOutputStream out, String string) throws IOException {
		byte bytes[] = string.getBytes();
		out.writeByte(bytes.length);
		out.write(bytes);
	}

	public static void restoreShops() {
		Shop shop;
		for (Iterator<Shop> iterator = handledShops.values().iterator(); iterator.hasNext(); shop.restoreItems())
			shop = (Shop) iterator.next();

	}

	public static boolean openShop(Player player, int key) {
		Shop shop = getShop(key);
		if (shop == null) {
			return false;
		} else {
			shop.addPlayer(player);
			return true;
		}
	}

	@SuppressWarnings("unused")
	public static boolean openrfdShop(Player player, int key) {
		Shop shop = getShop(key);
		boolean generalStore = true;
		// addShop(key, new Shop(7462, 10000, 10000, generalStore));
		shop.addPlayer(player);
		return true;
	}

	public static Shop getShop(int key) {
		return (Shop) handledShops.get(Integer.valueOf(key));
	}

	public static void addShop(int key, Shop shop) {
		handledShops.put(Integer.valueOf(key), shop);
	}

	private static final HashMap<Integer, Shop> handledShops = new HashMap<Integer, Shop>();
	@SuppressWarnings("unused")
	private static final String PACKED_PATH = "data/items/packedShops.s";
	private static final String UNPACKED_PATH = "data/items/unpackedShops.txt";

}