package es.thalesalv.runescape.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

import es.thalesalv.runescape.exception.ClanNotFoundError;
import es.thalesalv.runescape.exception.PlayerNotFoundError;
import es.thalesalv.runescape.exception.SaveError;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.utils.Logger;
import es.thalesalv.runescape.game.player.content.clans.Clan;
import es.thalesalv.runescape.game.player.content.grandExchange.Offer;
import es.thalesalv.runescape.game.player.content.grandExchange.OfferHistory;

public class SerializableFilesManager {

	public static final String PATH = "data/characters/";
	private static final String BACKUP_PATH = "data/charactersBackup/";
	private static final String CLAN_PATH = "data/clans/";
	private static final String GE_OFFERS = "data/grandExchangeOffers.ser";
	private static final String GE_OFFERS_HISTORY = "data/grandExchangeOffersTrack.ser";
	private static final String GE_PRICES = "data/grandExchangePrices.ser";

	public synchronized static final boolean containsPlayer(String username) {
		return new File(PATH + username + ".p").exists();
	}

	public synchronized static Player loadPlayer(String username) throws PlayerNotFoundError {
		try {
			return (Player) loadSerializedFile(new File(PATH + username + ".p"));
		} catch (Throwable e) {
			throw new PlayerNotFoundError(e.getMessage(), e);
		}
		
		/*try {
			Logger.log("SerializableFilesManager", "Recovering account: " + username);
			return (Player) loadSerializedFile(new File(BACKUP_PATH + username + ".p"));
		} catch (Throwable e) {
			Logger.handle((PlayerNotFoundError) e);
		}
		
		return null;*/
	}

	public static boolean createBackup(String username) {
		try {
			Utils.copyFile(new File(PATH + username + ".p"), new File(BACKUP_PATH + username + ".p"));
			return true;
		} catch (Throwable e) {
			Logger.handle((PlayerNotFoundError) e);
			return false;
		}
	}

	public synchronized static void savePlayer(Player player) throws PlayerNotFoundError {
		try {
			storeSerializableClass(player, new File(PATH + player.getUsername() + ".p"));
		} catch (ConcurrentModificationException e) {
			Logger.handle(e);
		} catch (Throwable e) {
			throw new PlayerNotFoundError(e.getMessage(), e);
		}
	}

	public static final Object loadSerializedFile(File f) throws IOException, ClassNotFoundException {
		if (!f.exists())
			return null;
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
		Object object = in.readObject();
		in.close();
		return object;
	}

	public static final void storeSerializableClass(Serializable o, File f) throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
		out.writeObject(o);
		out.close();
	}

	private SerializableFilesManager() {

	}

	public synchronized static boolean containsClan(String name) {
		return new File(CLAN_PATH + name + ".c").exists();
	}

	public synchronized static Clan loadClan(String name) throws ClanNotFoundError {
		try {
			return (Clan) loadSerializedFile(new File(CLAN_PATH + name + ".c"));
		} catch (Throwable e) {
			throw new ClanNotFoundError(e.getMessage(), e);
		}
	}

	public synchronized static void saveClan(Clan clan) throws SaveError {
		try {
			storeSerializableClass(clan, new File(CLAN_PATH + clan.getClanName() + ".c"));
		} catch (Throwable e) {
			throw new SaveError(e.getMessage(), e);
		}
	}

	public synchronized static void deleteClan(Clan clan) throws ClanNotFoundError {
		try {
			new File(CLAN_PATH + clan.getClanName() + ".c").delete();
		} catch (Throwable e) {
			throw new ClanNotFoundError(e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	public static synchronized HashMap<Long, Offer> loadGEOffers() {
		if (fileExists(GE_OFFERS)) {
			try {
				return (HashMap<Long, Offer>) SerializableFilesManager.loadSerializedFile(new File(GE_OFFERS));
			} catch (Throwable t) {
				Logger.handle(t);
				return null;
			}
		} else {
			return new HashMap<Long, Offer>();
		}
	}

	@SuppressWarnings("unchecked")
	public static synchronized ArrayList<OfferHistory> loadGEHistory() {
		if (fileExists(GE_OFFERS_HISTORY)) {
			try {
				return (ArrayList<OfferHistory>) SerializableFilesManager
						.loadSerializedFile(new File(GE_OFFERS_HISTORY));
			} catch (Throwable t) {
				Logger.handle(t);
				return null;
			}
		} else {
			return new ArrayList<OfferHistory>();
		}
	}

	@SuppressWarnings("unchecked")
	public static synchronized HashMap<Integer, Integer> loadGEPrices() {
		if (fileExists(GE_PRICES)) {
			try {
				return (HashMap<Integer, Integer>) SerializableFilesManager.loadSerializedFile(new File(GE_PRICES));
			} catch (Throwable t) {
				Logger.handle(t);
				return null;
			}
		} else {
			return new HashMap<Integer, Integer>();
		}
	}

	public static synchronized void saveGEOffers(HashMap<Long, Offer> offers) {
		try {
			SerializableFilesManager.storeSerializableClass(offers, new File(GE_OFFERS));
		} catch (Throwable t) {
			Logger.handle(t);
		}
	}

	public static synchronized void saveGEHistory(ArrayList<OfferHistory> history) {
		try {
			SerializableFilesManager.storeSerializableClass(history, new File(GE_OFFERS_HISTORY));
		} catch (Throwable t) {
			Logger.handle(t);
		}
	}

	public static synchronized void saveGEPrices(HashMap<Integer, Integer> prices) {
		try {
			SerializableFilesManager.storeSerializableClass(prices, new File(GE_PRICES));
		} catch (Throwable t) {
			Logger.handle(t);
		}
	}

	public static boolean fileExists(String dir) {
		return new File(dir).exists();
	}

}
