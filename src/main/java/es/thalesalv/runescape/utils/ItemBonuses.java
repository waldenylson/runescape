package es.thalesalv.runescape.utils;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.HashMap;

import es.thalesalv.runescape.exception.InitializationError;

public final class ItemBonuses {

	private static HashMap<Integer, int[]> itemBonuses;
	private final static String PACKED_PATH = "data/items/bonuses.ib";

	public static final void init() throws InitializationError {
		try {
			if (new File(PACKED_PATH).exists()) {
				loadItemBonuses();
			} else {
				throw new InitializationError("Missing item bonuses.");
			}
		} catch (Exception e) {
			throw new InitializationError(e.getMessage(), e);
		}
	}

	public static final int[] getItemBonuses(int itemId) {
		return itemBonuses.get(itemId);
	}

	public static final void reload() {
		itemBonuses.clear();
		try {
			init();
		} catch (InitializationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static final void loadItemBonuses() {
		try {
			RandomAccessFile in = new RandomAccessFile(PACKED_PATH, "r");
			FileChannel channel = in.getChannel();
			ByteBuffer buffer = channel.map(MapMode.READ_ONLY, 0, channel.size());
			itemBonuses = new HashMap<Integer, int[]>(buffer.remaining() / 38);
			while (buffer.hasRemaining()) {
				int itemId = buffer.getShort() & 0xffff;
				int[] bonuses = new int[18];
				for (int index = 0; index < bonuses.length; index++)
					bonuses[index] = buffer.getShort();
				itemBonuses.put(itemId, bonuses);
			}
			channel.close();
			in.close();
		} catch (Throwable e) {
			Logger.handle(e);
		}

	}

	private ItemBonuses() {

	}

}
