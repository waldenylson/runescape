package es.thalesalv.runescape.utils;

import java.io.File;
import java.util.concurrent.CopyOnWriteArrayList;

import es.thalesalv.runescape.exception.InitializationError;
import es.thalesalv.runescape.exception.SaveError;
import es.thalesalv.runescape.game.player.Player;

public final class IPBanL {

	public static CopyOnWriteArrayList<String> ipList;

	private static final String PATH = "data/bannedIPS.ser";
	private static boolean edited;

	@SuppressWarnings("unchecked")
	public static void init() throws InitializationError {
		File file = new File(PATH);
		if (file.exists())
			try {
				ipList = (CopyOnWriteArrayList<String>) SerializableFilesManager.loadSerializedFile(file);
				return;
			} catch (Throwable e) {
				throw new InitializationError(e.getMessage(), e);
			}
		ipList = new CopyOnWriteArrayList<String>();
	}

	public static final void save() throws SaveError {
		if (!edited)
			return;
		try {
			SerializableFilesManager.storeSerializableClass(ipList, new File(PATH));
			edited = false;
		} catch (Throwable e) {
			throw new SaveError(e.getMessage(), e);
		}
	}

	public static boolean isBanned(String ip) {
		return ipList.contains(ip);
	}

	public static void ban(Player player, boolean loggedIn) {
		player.setPermBanned(true);
		if (loggedIn) {
			ipList.add(player.getSession().getIP());
			ipList.add(player.getMacAddress());
			player.getSession().getChannel().disconnect();
		} else {
			try {
				ipList.add(player.getLastIP());
				SerializableFilesManager.savePlayer(player);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		edited = true;
	}

	public static void unban(Player player) {
		try {
			player.setPermBanned(false);
			player.setBanned(0);
			ipList.remove(player.getLastIP());
			ipList.remove(player.getMacAddress());
			edited = true;
			save();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void checkCurrent() {
		for (String list : ipList) {
			System.out.println(list);
		}
	}

	public static CopyOnWriteArrayList<String> getList() {
		return ipList;
	}

}
