package es.thalesalv.runescape.util;

import java.sql.Connection;
import java.sql.DriverManager;

import es.thalesalv.runescape.exception.DatabaseConnectionError;
import es.thalesalv.runescape.utils.Logger;

public class DatabaseConnection {

	public static Connection getConnection() throws Exception {
		Logger.log("Database", "Getting database connection...");
		Connection conn = null;

		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(Properties.DB_STRING, Properties.DB_USER, Properties.DB_PASSWD);
		} catch (Exception e) {
			throw new DatabaseConnectionError(e.getMessage(), e);
		}

		return conn;
	}
}
