package es.thalesalv.runescape.util;

public class Properties {

	/* Database Properties */
	public static final String DB_STRING = "jdbc:postgresql://127.0.0.1:5432/runescape";
	public static final String DB_USER = "runescape";
	public static final String DB_PASSWD = "runescape";
	
	/* Coordinates */
	public static final Integer PLAYER_START_X = 3222;
	public static final Integer PLAYER_START_Y = 3218;
	public static final Integer PLAYER_START_PLANE = 0;
	
	/* Rates */
	public static Integer XP_RATE = 50;
	public static Integer COMBAT_XP_RATE = 50;
	public static final Integer DONOR_MULT = 2;
	public static final Integer DROP_RATE = 1;
}
