package es.thalesalv.runescape.game.task.impl;

import es.thalesalv.runescape.Launcher;
import es.thalesalv.runescape.game.task.Task;
//import es.thalesalv.runescape.game.player.content.controler.impl.PestControler;
//import es.thalesalv.runescape.game.player.content.controler.impl.SoulwarsControler;
//import es.thalesalv.runescape.game.player.content.minigames.impl.PestControl;
//import es.thalesalv.runescape.game.player.content.minigames.impl.Soulwars;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.OwnedObjectManager;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.utils.Logger;
import es.thalesalv.runescape.utils.ServerMessages;

public class MinuteTask extends Task {

	private boolean checkAgility;
	private int count;

	private final static int TICK_DELAY = 100;

	public MinuteTask() {
		super(TICK_DELAY, true, TickType.MAIN);
	}

	@Override
	protected void execute() {
		try {



			for (Player player : World.getPlayers()) {

				if (player == null)
					continue;

				/*if ((player.isInSoulwarsLobby || player.isInSoulwarsGame) && Soulwars.startedGame) {
					SoulwarsControler.playTimer--;
				}

				if (player.isInSoulwarsLobby && !Soulwars.startedGame) {
					SoulwarsControler.waitTimer--;

					if (player.isInSoulwarsLobby == true && Soulwars.startedGame == false) {
						SoulwarsControler.waitTimer--;

						if (player.isInPestControlLobby == true && PestControl.startedGame == false) {
							PestControler.waitTimer--;
						}
						if (player.isInPestControlGame == true && PestControl.startedGame == true) {
							PestControler.playTimer--;
						}
					}
				}*/
			}

			OwnedObjectManager.processAll();

			checkAgility = !checkAgility;
			count++;

		} catch (Exception e) {
			Logger.handle(e);
		}

	}

}
