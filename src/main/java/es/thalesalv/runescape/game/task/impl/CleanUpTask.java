package es.thalesalv.runescape.game.task.impl;

import es.thalesalv.runescape.Launcher;
import es.thalesalv.runescape.Settings;
import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.cache.loaders.ItemDefinitions;
import es.thalesalv.runescape.cache.loaders.NPCDefinitions;
import es.thalesalv.runescape.cache.loaders.ObjectDefinitions;
import es.thalesalv.runescape.game.GameEngine;
import es.thalesalv.runescape.game.task.Task;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.Region;
import es.thalesalv.runescape.utils.Logger;
import com.alex.store.Index;

public class CleanUpTask extends Task {

	private final static int TICK_DELAY = 1000;

	public CleanUpTask() {
		super(TICK_DELAY, true, TickType.MAIN);
	}

	@Override
	protected void execute() {
		try {

			if (Runtime.getRuntime().freeMemory() < Settings.MIN_FREE_MEM_ALLOWED) {
				ItemDefinitions.clearItemsDefinitions();
				NPCDefinitions.clearNPCDefinitions();
				ObjectDefinitions.clearObjectDefinitions();
				/*for (Region region : World.getRegions().values())
					region.removeMapFromMemory();*/
			}

			for (Index index : Cache.STORE.getIndexes())
				index.resetCachedFiles();

			GameEngine.get().fastExecutor().purge();
			System.gc();

		} catch (Exception e) {
			Logger.handle(e);
		}
	}

}
