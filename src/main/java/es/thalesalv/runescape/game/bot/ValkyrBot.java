package es.thalesalv.runescape.game.bot;

import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.net.Session;

@SuppressWarnings("serial")
public class ValkyrBot extends Player {
	
	public ValkyrBot(String password) {
		super(password);
		final Session session = new Session(null);
		init(session, "valkyr Bot_" + World.getPlayers().size(), 0, 0, 0, null, null);
		session.getLoginPackets().sendLoginDetails(this);
		session.setDecoder(3, this);
		session.setEncoder(2, this);
		start();
	}
	
}
