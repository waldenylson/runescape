package es.thalesalv.runescape.game.player.dialogues;

import es.thalesalv.runescape.Settings;
import es.thalesalv.runescape.cache.loaders.NPCDefinitions;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.minigames.CastleWars;
import es.thalesalv.runescape.game.player.Skills;
import es.thalesalv.runescape.game.player.content.magic.Magic;
import es.thalesalv.runescape.game.player.controlers.FightCaves;
import es.thalesalv.runescape.game.player.controlers.FightKiln;
import es.thalesalv.runescape.utils.ShopsHandler;

public class DonatorShops extends Dialogue {

	private int npcId;

	@Override
	public void start() {
	
		sendOptionsDialogue("Donator Shops",
				"Donator Shops", "Exclusive Shops",
				"None");
		stage = 1;

	}

	@Override
	public void run(int interfaceId, int componentId) {
		if (stage == 1) {
			if (componentId == OPTION_1) { //General Store
				player.getInterfaceManager().closeChatBoxInterface();
				player.getInterfaceManager().closeOverlay(true);
				ShopsHandler.openShop(player, 24);
			} else if (componentId == OPTION_2) {//Combat
				sendOptionsDialogue("Exclusive Shops", "Potion Flasks",
						"Steel Titan");
				stage = 2;
			} else if (componentId == OPTION_3) {//Skilling
				end();
			}
		} else if (stage == 2) {
			if (componentId == OPTION_1) { //Flasks
				player.getInterfaceManager().closeChatBoxInterface();
				player.getInterfaceManager().closeOverlay(true);
				ShopsHandler.openShop(player, 10);
			} else if (componentId == OPTION_2) {//Steel titan scrolls
				player.getInterfaceManager().closeChatBoxInterface();
				player.getInterfaceManager().closeOverlay(true);
				ShopsHandler.openShop(player, 22);
			}
		}
			
			
			/**
			 * 
			 */
			
			
		
	}
			@Override
			public void finish() {
			}
	

}