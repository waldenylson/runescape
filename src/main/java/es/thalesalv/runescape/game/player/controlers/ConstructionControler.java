package es.thalesalv.runescape.game.player.controlers;

import es.thalesalv.runescape.game.RegionBuilder;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.content.construction.House;

public class ConstructionControler extends Controler {
	
	private House house;
	private int[] boundChuncks;
	
	@Override
	public void start() {
		//house = new House();
		boundChuncks = RegionBuilder.findEmptyChunkBound(8, 8); 
		//house.initHouse(boundChuncks, true);
		player.setNextWorldTile(new WorldTile(boundChuncks[0]*8 + 35, boundChuncks[1]*8 + 35,0));
		player.getPackets().sendGameMessage("Welcome to your house! Make a bed or wardrobe!");
	}
	
	boolean remove = true;
}