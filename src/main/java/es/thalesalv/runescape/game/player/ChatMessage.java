package es.thalesalv.runescape.game.player;

import es.thalesalv.runescape.utils.Censor;
import es.thalesalv.runescape.utils.Utils;

public class ChatMessage {

    private String message;
    private String filteredMessage;

    public ChatMessage(String message) {
	    filteredMessage = Censor.getFilteredMessage(message);
	    this.message = Utils.fixChatMessage(message);
    }

    public String getMessage(boolean filtered) {
	return filtered ? filteredMessage : message;
    }
}
