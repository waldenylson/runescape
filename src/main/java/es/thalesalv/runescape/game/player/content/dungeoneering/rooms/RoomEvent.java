package es.thalesalv.runescape.game.player.content.dungeoneering.rooms;

import es.thalesalv.runescape.game.player.content.dungeoneering.DungeonManager;
import es.thalesalv.runescape.game.player.content.dungeoneering.RoomReference;

public interface RoomEvent {

	public void openRoom(DungeonManager dungeon, RoomReference reference);
}
