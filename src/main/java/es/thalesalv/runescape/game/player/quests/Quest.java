package es.thalesalv.runescape.game.player.quests;

import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.quests.QuestManager.Progress;

/**
 * A extendable quest controler.
 * 
 * @author Apache Ah64
 */
public abstract class Quest {

	/**
	 * The player reference.
	 */
	protected Player player;

	/**
	 * The quest id.
	 */
	protected int questId;

	/**
	 * The quest progress.
	 */
	protected Progress progress;

	/**
	 * Start the quest.
	 */
	public abstract void start();

	/**
	 * Finish the quest.
	 */
	public abstract void finish();
}