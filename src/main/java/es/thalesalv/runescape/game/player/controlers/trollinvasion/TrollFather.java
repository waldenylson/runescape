package es.thalesalv.runescape.game.player.controlers.trollinvasion;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;


public class TrollFather extends CombatScript {

	@Override
	public Object[] getKeys() {
		return new Object[] { "Troll father" };
	}

	@Override
	public int attack(NPC npc, Entity target) {
		final NPCCombatDefinitions defs = npc.getCombatDefinitions();
		int hit = 0;
		hit = getRandomMaxHit(npc, 250, NPCCombatDefinitions.MELEE, target);
		delayHit(npc, 2, target, getMeleeHit(npc, hit));
		return defs.getAttackDelay();
	}

}
