package es.thalesalv.runescape.game.player.content;

import es.thalesalv.runescape.game.player.Player;

public final class AdventurersLog {

	private AdventurersLog() {
		
	}
	
	public static void open(Player player) {
		player.getInterfaceManager().sendInterface(623);
	}
}
