package es.thalesalv.runescape.game.player.dialogues;

import es.thalesalv.runescape.game.player.dialogues.Dialogue;
import es.thalesalv.runescape.utils.ShopsHandler;

public class DeathTaskMaster extends Dialogue {

    private int npcId;

    @Override
    public void start() {
	npcId = (Integer) parameters[0];
	sendNPCDialogue(npcId, 9827, "Greetings " + player.getDisplayName() + ", are you after death?!");
	    stage = 0;
    }

    @Override
    public void run(int interfaceId, int componentId) {
		if (stage == 0) {
		    sendNPCDialogue(npcId, 9827, "Here take a Death Task.");
		    player.getDeathsManager().getTask(0);
		    player.sendMessage("You must kill "+player.getDeathsManager().getAmount()+" "+player.getDeathsManager().getName()+"'s.");	
		    stage = 2;
		} else if (stage == 2) {
			end();
		} else {
			end();
		}
	}

    @Override
    public void finish() {

    }

}
