package es.thalesalv.runescape.game.player.actions.objects;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.WorldObject;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.controlers.Barrows;

/*
 * @Author Danny
 * Varrock City
 */

public class BarrowsObjects {
	
	public static void Spades(Player player,
			final WorldObject object) {
		player.setNextAnimation(new Animation(830));
		if (Barrows.digIntoGrave(player))
			return;
	}
	
	

	public static boolean isObject(final WorldObject object) {
		switch (object.getId()) {
		case 66115:
		case 66116:
		return true;
		default:
		return false;
		}
	}
	
	public static void HandleObject(Player player, final WorldObject object) {
		final int id = object.getId();
		if (id == 66115 || id == 66116) { 
			BarrowsObjects.Spades(player, object); 
		}
	}

}