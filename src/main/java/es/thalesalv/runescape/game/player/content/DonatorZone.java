package es.thalesalv.runescape.game.player.content;

import java.util.List;

import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.magic.Magic;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

public class DonatorZone {

	public static void enterDonatorzone(final Player player) {
		Magic.sendNormalTeleportSpell(player, 0, 0, new WorldTile(2833, 3854, 0));
		for (int regionId : player.getMapRegionsIds()) {
			List<Integer> npcIndexes = World.getRegion(regionId).getNPCsIndexes();
			if (npcIndexes != null) {
				for (int npcIndex : npcIndexes) {
					final NPC n = World.getNPCs().get(npcIndex);
					if (n == null || n.getId() != 5445)
						continue;
					WorldTasksManager.schedule(new WorldTask() {
						@Override
						public void run() {
							final int random = Utils.getRandom(3);
							if (random == 0)
								n.setNextForceTalk(new ForceTalk("Everyone welcome "+player.getDisplayName()+" to the donator zone."));
							else if (random == 1)
								n.setNextForceTalk(new ForceTalk(player.getDisplayName()+" has just joined the penguin zone."));
							else if (random == 2)
								n.setNextForceTalk(new ForceTalk("Ma boi "+player.getDisplayName()+" has just joined the penguin zone."));
							else if (random == 3)
								n.setNextForceTalk(new ForceTalk("Who else wouldnt want "+player.getDisplayName()+" from joining the penguin zone."));
						}
					}, 4);
				}
			}
		}
	}
}
