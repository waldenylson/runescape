package es.thalesalv.runescape.game.player.cutscenes.actions;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.cutscenes.Cutscene;

public class NPCFaceTileAction extends CutsceneAction {

	private int x, y;

	public NPCFaceTileAction(int cachedObjectIndex, int x, int y,
			int actionDelay) {
		super(cachedObjectIndex, actionDelay);
		this.x = x;
		this.y = y;
	}

	@Override
	public void process(Player player, Object[] cache) {
		Cutscene scene = (Cutscene) cache[0];
		NPC npc = (NPC) cache[getCachedObjectIndex()];
		npc.setNextFaceWorldTile(new WorldTile(scene.getBaseX() + x, scene
				.getBaseY() + y, npc.getPlane()));
	}

}
