package es.thalesalv.runescape.game.player.content;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;

public class LavaFlowMines {
	
	static WorldTile INSIDE = new WorldTile(2177, 5664, 0);
	
	public static void Entering(Player player) {
		player.stopAll();
		player.setNextWorldTile(INSIDE);
		player.getDialogueManager().startDialogue("SimpleNPCMessage", 13395, "Welcome to the mines " +player.getDisplayName() + "!");
		
	}
	
}
