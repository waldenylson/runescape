package es.thalesalv.runescape.game.player.dialogues;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.Settings;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Skills;
import es.thalesalv.runescape.game.player.content.Magic;
import es.thalesalv.runescape.utils.ShopsHandler;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.DonatorZone;

public class DonationStore extends Dialogue {

	@Override
	public void start() {
		sendOptionsDialogue("Select an option.",
				"Donator Zone", "Vip Zone", "Donation Page", "Close");
	}

	@Override
	public void run(int interfaceId, int componentId) {
		if (componentId == OPTION_1) {
		if (player.isDonator()) {
					DonatorZone.enterDonatorzone(player);
				}
			}
		if (componentId == OPTION_2) {
		if (player.isSupremeDonator()) {
                
          Magic.sendPegasusTeleportSpell(player, 0, 0, new WorldTile(1824, 5146, 2));
		 
			}
			}
		if (componentId == OPTION_3) {
		player.getPackets().sendOpenURL(Settings.DONATE_LINK);
			}		
			
			
	    if (componentId == OPTION_4) {
			end();
		}
		end();
		
		}

	

	@Override
	public void finish() {

	}

}
