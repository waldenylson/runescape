package es.thalesalv.runescape.game.player.controlers;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.WorldObject;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.content.magic.Magic;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;

/**
 * Handles Random Event Controler
 * 
 * @author Demon Dylan
 *
 */

public class QuizEvent extends Controler {


	@Override
	public void start() {
	//todo
	}
	@Override
	public boolean sendDeath() {
		player.addStopDelay(7);
		player.stopAll();
		WorldTasksManager.schedule(new WorldTask() {
			int loop;

			@Override
			public void run() {
				if (loop == 0) {
					player.setNextAnimation(new Animation(836));
				} else if (loop == 1) {
					player.getPackets().sendGameMessage(
							"Oh dear, you have died.");
				} else if (loop == 3) {
					player.reset();
					player.setNextWorldTile(new WorldTile(2602, 4775, 0));
					player.setNextAnimation(new Animation(-1));
					stop();
				}
				loop++;
			}
		}, 0, 1);
		return false;
	}


	@Override
	public boolean login() {
		player.getDialogueManager().startDialogue("QuizMaster", 2477);
		return false;
	}

	@Override
	public boolean logout() {
		return false;
	}

	@Override
	public boolean processMagicTeleport(WorldTile toTile) {
		player.getPackets().sendGameMessage("You can't teleport in this area!");
		return false;
	}

	@Override
	public boolean processItemTeleport(WorldTile toTile) {
		player.getPackets().sendGameMessage("You can't teleport in this area!");
		return false;
	}

	@Override
	public boolean processObjectClick1(WorldObject object) {
		if (object.getId() == 15645)
			return true;
		return false;
	}

}