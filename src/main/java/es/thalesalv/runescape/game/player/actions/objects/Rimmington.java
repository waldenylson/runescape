package es.thalesalv.runescape.game.player.actions.objects;

import es.thalesalv.runescape.game.WorldObject;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.magic.Magic;

/*
 * @Author Danny
 * Rimmington
 */

public class Rimmington {
	
	public static void Example(Player player,
			final WorldObject object) {
		//Insert what the object does here
	}

	public static boolean isObject(final WorldObject object) {
		switch (object.getId()) {
		default:
		return false;
		}
	}
	
	public static void HandleObject(Player player, final WorldObject object) {
		final int id = object.getId();
		if (id == 16154) { //Object ID
			Rimmington.Example(player, object); //Method of Action
		}
		
	}

}
