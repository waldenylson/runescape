package es.thalesalv.runescape.game.npc.dung;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.content.dungeoneering.DungeonManager;
import es.thalesalv.runescape.game.player.content.dungeoneering.RoomReference;

@SuppressWarnings("serial")
public class Guardian extends NPC {

	private DungeonManager manager;
	private RoomReference reference;

	public Guardian(int id, WorldTile tile, DungeonManager manager,
			RoomReference reference) {
		super(id, tile, -1, true, true);
		this.manager = manager;
		this.reference = reference;
	}

	@Override
	public void sendDeath(Entity source) {
		super.sendDeath(source);
		manager.updateGuardian(reference);
	}

}
