package es.thalesalv.runescape.game.npc.others;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

@SuppressWarnings("serial")
public class Werewolf extends NPC {

    private int realId;

    public Werewolf(int id, WorldTile tile, int mapAreaNameHash, boolean canBeAttackFromOutOfArea, boolean spawned) {
	super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea, spawned);
	realId = id;
    }

    public boolean hasWolfbane(Entity target) {
	if(target instanceof NPC)
	    return false;
	return ((Player) target).getEquipment().getWeaponId() == 2952;
    }
    @Override
    public void processNPC() {
	if (isDead() || isCantInteract())
	    return;
	if(isUnderCombat() && getId() == realId && Utils.random(5) == 0) {
	    final Entity target = getCombat().getTarget();
	    if(!hasWolfbane(target)) {
		    setNextAnimation(new Animation(6554));
		    setCantInteract(true);
		    WorldTasksManager.schedule(new WorldTask() {
			@Override
			public void run() {
			    setNextNPCTransformation(realId-20);
			    setNextAnimation(new Animation(-1));
			    setCantInteract(false);
			    setTarget(target);
			}
		    }, 1);
		    return;
	    }
	}
	super.processNPC();
    }

    @Override
    public void reset() {
	setNPC(realId);
	super.reset();
    }

}
