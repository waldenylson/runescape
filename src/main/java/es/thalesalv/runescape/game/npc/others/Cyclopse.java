package es.thalesalv.runescape.game.npc.others;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.minigames.WarriorsGuild;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.godwars.bandos.GodwarsBandosFaction;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.controlers.Controler;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

@SuppressWarnings("serial")
public class Cyclopse extends GodwarsBandosFaction {

    public Cyclopse(int id, WorldTile tile, int mapAreaNameHash, boolean canBeAttackFromOutOfArea) {
	super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea, false);
    }

    @Override
    public void sendDeath(Entity source) {
	super.sendDeath(source);
	if (source instanceof Player) {
	    WarriorsGuild.killedCyclopses++;
	    final NPC npc = this;
	    final Player player = (Player) source;
	    Controler controler = player.getControlerManager().getControler();
	    if (controler == null || !(controler instanceof WarriorsGuild) || Utils.random(15) != 0) 
		return;
	    WorldTasksManager.schedule(new WorldTask() {

		@Override
		public void run() {
		    World.addGroundItem(new Item(WarriorsGuild.getBestDefender(player)), new WorldTile(getCoordFaceX(npc.getSize()), getCoordFaceY(npc.getSize()), getPlane()), player, true, 60);
		}
	    }, getCombatDefinitions().getDeathDelay());
	}
    }
}
