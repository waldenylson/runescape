package es.thalesalv.runescape.game.npc.combat.impl;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.utils.Utils;

/**
 * 
 * @author Mario(AlterOPSnet)
 * 
 *
 */

public class FireWizardCombat extends CombatScript {

	@Override
	public Object[] getKeys() {
		return new Object[] { 2709 };
	}
	
	@Override
	public int attack(final NPC npc, final Entity target) {
		final NPCCombatDefinitions defs = npc.getCombatDefinitions();
		
		npc.setNextGraphics(new Graphics(2728));
		npc.setNextAnimation(new Animation(14223));
		
		World.sendProjectile(npc,
								target,
										2735, 40, 40, 50, 50, 0, 0);
		delayHit(npc,
				14,
					target,
						getMagicHit(npc, Utils.random(40)));
		target.setNextGraphics(new Graphics(2739, 100, 100));
		
				return defs.getAttackDelay();

	}
}