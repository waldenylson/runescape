package es.thalesalv.runescape.game.npc.combat.impl;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.Slayer;
import es.thalesalv.runescape.utils.Utils;

public class BansheeCombat extends CombatScript {

    @Override
    public Object[] getKeys() {
	return new Object[] {"Banshee", "Mighty banshee"};
    }
    
    
    
    
    @Override
    public int attack(NPC npc, Entity target) {
	NPCCombatDefinitions def = npc.getCombatDefinitions();
	if (!Slayer.hasEarmuffs(target)) {
	    	Player targetPlayer = (Player) target;
			int randomSkill = Utils.random(0, 2);
			int currentLevel = targetPlayer.getSkills().getLevel(randomSkill);
			targetPlayer.getSkills().set(randomSkill, currentLevel < 5 ? 0 : currentLevel - 5);
			targetPlayer.getPackets().sendGameMessage("The screams of the banshee make you feel slightly weaker.");
			npc.setNextForceTalk(new ForceTalk("*EEEEHHHAHHH*"));
			delayHit(npc, 0, target, getMeleeHit(npc, targetPlayer.getMaxHitpoints() / 10));
			//TODO player emote hands on ears
	}else
	    delayHit(npc, 0, target, getMeleeHit(npc, getRandomMaxHit(npc, npc.getMaxHit(), def.getAttackStyle(), target)));
	npc.setNextAnimation(new Animation(def.getAttackEmote()));
	return def.getAttackDelay();
    }
}
