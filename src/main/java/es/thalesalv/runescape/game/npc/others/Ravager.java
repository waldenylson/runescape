package es.thalesalv.runescape.game.npc.others;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;

@SuppressWarnings("serial")
public class Ravager extends NPC {
	
	boolean destroyingObject = false;

	public Ravager(int id, WorldTile tile, int mapAreaNameHash,
			boolean canBeAttackFromOutOfArea) {
		super(id, tile, -1, false, false);
	}
	
	@Override
	public void processNPC() {
	}

}
