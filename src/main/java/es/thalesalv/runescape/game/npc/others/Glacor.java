package es.thalesalv.runescape.game.npc.others;

import java.util.concurrent.TimeUnit;

import es.thalesalv.runescape.cores.CoresManager;
import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;

@SuppressWarnings("serial")
public class Glacor extends NPC {

	
	public Glacor(int id, WorldTile tile, int mapAreaNameHash,
			boolean canBeAttackFromOutOfArea, boolean spawned) {
		super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea, spawned);
		setLureDelay(0);
		setRun(true);
		setForceTargetDistance(64);
		setForceMultiAttacked(true);
	}
	
	@Override
	public void handleIngoingHit(final Hit hit) {
		super.handleIngoingHit(hit);
		if (hit.getSource() instanceof Player) {
			Player player = (Player) hit.getSource();
			if (hit.getLook() == HitLook.MELEE_DAMAGE || hit.getLook() == HitLook.RANGE_DAMAGE) {
				hit.setDamage(0);
				player.getPackets().sendGameMessage("Glacors can only be harmed by magic.");
			}
		}
	}
	
}