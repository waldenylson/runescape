package es.thalesalv.runescape.game.npc.combat.impl;

import java.util.ArrayList;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.game.npc.corp.CorporealBeast;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.Skills;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

public class GlacorCombat extends CombatScript{

	public Object[] getKeys() {
		return (new Object[]{Integer.valueOf(14301)});
	}

	public NPC Glacor;
	
	@Override
	public int attack(NPC npc, Entity target) {
		final NPCCombatDefinitions defs = npc.getCombatDefinitions();
        int distanceX = target.getX() - npc.getX();
        int distanceY = target.getY() - npc.getY();
		Player player = (Player) target;
			switch (Utils.getRandom(3)) {
			case 0:
				if (distanceX < 15 || distanceY < 15){
					npc.setNextAnimation(new Animation(9968));
					npc.setNextGraphics(new Graphics(902));
					delayHit(npc, 0, player, new Hit(npc, Utils.random(450), HitLook.MAGIC_DAMAGE));
					World.sendProjectile(npc, target, 963, 34, 16, 40, 35, 16, 0);
				}
				break;
			case 1:
				if (distanceX < 15 || distanceY < 15){
					npc.setNextAnimation(new Animation(9968));
					npc.setNextGraphics(new Graphics(902));
					delayHit(npc, 0, player, new Hit(npc, Utils.random(320), HitLook.RANGE_DAMAGE));
					World.sendProjectile(npc, target, 962, 34, 16, 40, 35, 16, 0);
				}
				break;
			case 2:
				if (distanceX < 15 || distanceY < 15){
					npc.setNextAnimation(new Animation(9968));
					npc.setNextGraphics(new Graphics(902));
					delayHit(npc, 0, player, new Hit(npc, Utils.random(320), HitLook.MAGIC_DAMAGE));
					World.sendProjectile(npc, target, 963, 34, 16, 40, 35, 16, 0);
				}
				break;
			case 3:
				if (distanceX < 15 || distanceY < 15){
					npc.setNextAnimation(new Animation(9968));
					npc.setNextGraphics(new Graphics(902));
					delayHit(npc, 0, player, new Hit(npc, Utils.random(450), HitLook.RANGE_DAMAGE));
					World.sendProjectile(npc, target, 962, 34, 16, 40, 35, 16, 0);
				}
				break;
			}
		return defs.getAttackDelay();
	}
}