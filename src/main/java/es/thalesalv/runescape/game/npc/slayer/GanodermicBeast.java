package es.thalesalv.runescape.game.npc.slayer;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;

@SuppressWarnings("serial")
public class GanodermicBeast extends NPC {

	private boolean sprayed;

	public boolean isSprayed() {
		return sprayed;
	}

	public void setSprayed(boolean spray) {
		sprayed = spray;
	}

	public GanodermicBeast(int id, WorldTile tile, int mapAreaNameHash,
			boolean canBeAttackFromOutOfArea, boolean spawned) {
		super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea, spawned);
	}

	@Override
	public void handleIngoingHit(Hit hit) {
		int damage = hit.getDamage();
		if (hit.getLook() == HitLook.MAGIC_DAMAGE) {
			if (damage > 0) {
				damage *= (int) 1.75;
			}
		}
		hit.setDamage(damage);
		super.handleIngoingHit(hit);
	}

	public void startSpray(Player player) {
		if (isSprayed()) {
			player.getPackets().sendGameMessage(
					"This NPC has already been sprayed with neem oil.");
			return;
		}
		NPC before = this;
		transformIntoNPC(14697);
		setSprayed(true);
		this.setHitpoints(before.getHitpoints());
		setNextForceTalk(new ForceTalk("Rarghh"));
		WorldTasksManager.schedule(new WorldTask() {
			@Override
			public void run() {
				if (isDead()) {
					stop();
				}
				transformIntoNPC(14696);
				setSprayed(false);
			}
		}, 120);
	}

	@Override
	public void sendDeath(Entity source) {
		transformIntoNPC(14696);
		setSprayed(false);
		super.sendDeath(source);
	}
}