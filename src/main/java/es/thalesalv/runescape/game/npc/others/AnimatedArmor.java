package es.thalesalv.runescape.game.npc.others;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.minigames.WarriorsGuild;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

@SuppressWarnings("serial")
public class AnimatedArmor extends NPC {

    private transient Player player;
    
    public AnimatedArmor(Player player, int id, WorldTile tile, int mapAreaNameHash, boolean canBeAttackFromOutOfArea) {
	super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea);
	this.player = player;
    }

    @Override
    public void processNPC() {
	super.processNPC();
	if (!getCombat().underCombat())
	    finish();
    }

    @Override
    public void sendDeath(final Entity source) {
	final NPCCombatDefinitions defs = getCombatDefinitions();
	resetWalkSteps();
	getCombat().removeTarget();
	setNextAnimation(null);
	WorldTasksManager.schedule(new WorldTask() {
	    int loop;

	    @Override
	    public void run() {
		if (loop == 0) {
		    setNextAnimation(new Animation(defs.getDeathEmote()));
		} else if (loop >= defs.getDeathDelay()) {
		    if (source instanceof Player) {
			Player player = (Player) source;
			for (Integer items : getDroppedItems()) {
			    if (items == -1)
				continue;
			    World.addGroundItem(new Item(items), new WorldTile(getCoordFaceX(getSize()), getCoordFaceY(getSize()), getPlane()), player, true, 60);
			}
			player.setWarriorPoints(3, WarriorsGuild.ARMOR_POINTS[getId() - 4278]);
		    }
		    finish();
		    stop();
		}
		loop++;
	    }
	}, 0, 1);
    }

    public int[] getDroppedItems() {
	int index = getId() - 4278;
	int[] droppedItems = WarriorsGuild.ARMOUR_SETS[index];
	if (Utils.getRandom(15) == 0) // 1/15 chance of losing
	    droppedItems[Utils.random(0, 2)] = -1;
	return droppedItems;
    }

    @Override
    public void finish() {
	if (hasFinished())
	    return;
	super.finish();
	if (player != null) {
	    player.getTemporaryAttributtes().remove("animator_spawned");
	    if (!isDead()) {
		for (int item : getDroppedItems()) {
		    if (item == -1)
			continue;
		    player.getInventory().addItem(item, 1);
		}
	    }
	}
    }
}
