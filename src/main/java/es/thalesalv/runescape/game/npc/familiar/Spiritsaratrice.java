package es.thalesalv.runescape.game.npc.familiar;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.actions.Summoning.Pouches;
import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.player.Skills;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

public class Spiritsaratrice extends Familiar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6059371477618091701L;
	private int chocoTriceEgg;

	public Spiritsaratrice(Player owner, Pouches pouch, WorldTile tile,
			int mapAreaNameHash, boolean canBeAttackFromOutOfArea) {
		super(owner, pouch, tile, mapAreaNameHash, canBeAttackFromOutOfArea);
	}

    @Override
    public void processNPC() {
	super.processNPC();
	chocoTriceEgg++;
	if (chocoTriceEgg == 500)
	    addChocolateEgg();
    }

    private void addChocolateEgg() {
	getBob().getBeastItems().add(new Item(12109, 1));
	chocoTriceEgg = 0;
    }

    @Override
    public String getSpecialName() {
	return "Petrifying Gaze";
    }

    @Override
    public String getSpecialDescription() {
	return "Inflicts damage and drains a combat stat, which varies according to the type of cockatrice.";
    }

    @Override
    public int getBOBSize() {
	return 30;
    }

    @Override
    public int getSpecialAmount() {
	return 5;
    }

    @Override
    public SpecialAttack getSpecialAttack() {
	return SpecialAttack.ENTITY;
    }

    @Override
    public boolean submitSpecial(Object object) {
	final Entity target = (Entity) object;
	getOwner().setNextGraphics(new Graphics(1316));
	getOwner().setNextAnimation(new Animation(7660));
	setNextAnimation(new Animation(7766));
	setNextGraphics(new Graphics(1467));
	World.sendProjectile(this, target, 1468, 34, 16, 30, 35, 16, 0);
	if (target instanceof Player) {
	    Player playerTarget = (Player) target;
	    int level = playerTarget.getSkills().getLevelForXp(Skills.PRAYER);
	    int drained = 3;
	    if (level - drained > 0)
		drained = level;
	    playerTarget.getSkills().drainLevel(Skills.PRAYER, drained);
	}
	WorldTasksManager.schedule(new WorldTask() {

	    @Override
	    public void run() {
		target.applyHit(new Hit(getOwner(), Utils.random(100), HitLook.MELEE_DAMAGE));
	    }
	}, 2);
	return true;
    }
}