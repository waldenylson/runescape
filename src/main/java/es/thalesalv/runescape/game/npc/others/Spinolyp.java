package es.thalesalv.runescape.game.npc.others;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;

@SuppressWarnings("serial")
public class Spinolyp extends NPC {

	public Spinolyp(int id, WorldTile tile, int mapAreaNameHash, boolean canBeAttackFromOutOfArea, boolean spawned) {
		super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea, spawned);
	}
	
	@Override
	public void processNPC() {
		super.setCantFollowUnderCombat(true);
	}

}
