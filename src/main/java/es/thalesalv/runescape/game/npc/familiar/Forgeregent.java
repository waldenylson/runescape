package es.thalesalv.runescape.game.npc.familiar;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.actions.Summoning.Pouches;
import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.player.actions.PlayerCombat;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.net.decoders.handlers.ButtonHandler;
import es.thalesalv.runescape.utils.Utils;

public class Forgeregent extends Familiar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6059371477618091701L;

	public Forgeregent(Player owner, Pouches pouch, WorldTile tile,
			int mapAreaNameHash, boolean canBeAttackFromOutOfArea) {
		super(owner, pouch, tile, mapAreaNameHash, canBeAttackFromOutOfArea);
	}

	   @Override
	    public String getSpecialName() {
		return "Inferno";
	    }

	    @Override
	    public String getSpecialDescription() {
		return "A magical attack that disarms an enemy's weapon or shield.";
	    }

	    @Override
	    public int getBOBSize() {
		return 0;
	    }

	    @Override
	    public int getSpecialAmount() {
		return 6;
	    }

	    @Override
	    public SpecialAttack getSpecialAttack() {
		return SpecialAttack.ENTITY;
	    }

	    @Override
	    public boolean submitSpecial(Object object) {
		final Entity target = (Entity) object;
		getOwner().setNextGraphics(new Graphics(1316));
		getOwner().setNextAnimation(new Animation(7660));
		setNextAnimation(new Animation(7871));
		setNextGraphics(new Graphics(1394));
		WorldTasksManager.schedule(new WorldTask() {

		    @Override
		    public void run() {
			if (target instanceof Player) {
			    Player playerTarget = (Player) target;
			    int weaponId = playerTarget.getEquipment().getWeaponId();
			    if (weaponId != -1) {
				if (PlayerCombat.getWeaponAttackEmote(weaponId, 1) != 423) {
				    ButtonHandler.sendRemove(playerTarget, 3);
				}
			    }
			    int shieldId = playerTarget.getEquipment().getShieldId();
			    if (shieldId != -1) {
				ButtonHandler.sendRemove(playerTarget, 5);
			    }
			}
			target.setNextGraphics(new Graphics(1393));
			target.applyHit(new Hit(getOwner(), Utils.random(200), HitLook.MELEE_DAMAGE));
		    }
		}, 2);
		return true;
	    }
}