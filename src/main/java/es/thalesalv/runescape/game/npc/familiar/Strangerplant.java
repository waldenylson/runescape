package es.thalesalv.runescape.game.npc.familiar;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.actions.Summoning.Pouches;
import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.player.Skills;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Utils;

public class Strangerplant extends Familiar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6059371477618091701L;
	private int forageTicks;

	public Strangerplant(Player owner, Pouches pouch, WorldTile tile,
			int mapAreaNameHash, boolean canBeAttackFromOutOfArea) {
		super(owner, pouch, tile, mapAreaNameHash, canBeAttackFromOutOfArea);
		int currentLevel = owner.getSkills().getLevelForXp(Skills.FARMING);
		owner.getSkills().set(Skills.FARMING, (int) ((1 + (currentLevel * .04)) + currentLevel));
		owner.getPackets().sendGameMessage("You feel a sudden urge to plant flowers.");
	    }

	    @Override
	    public void processNPC() {
		super.processNPC();
		forageTicks++;
		if (forageTicks == 750)
		    addStrangeFruit();
	    }

	    private void addStrangeFruit() {
		getBob().getBeastItems().add(new Item(464, 1));
		forageTicks = 0;
	    }

	    @Override
	    public String getSpecialName() {
		return "Poisonous Blast";
	    }

	    @Override
	    public String getSpecialDescription() {
		return "Attack with 50% chance of poisoning your opponent and inflicting 20 damage";
	    }

	    @Override
	    public int getBOBSize() {
		return 30;
	    }

	    @Override
	    public int getSpecialAmount() {
		return 6;
	    }

	    @Override
	    public SpecialAttack getSpecialAttack() {
		return SpecialAttack.ENTITY;
	    }

	    @Override
	    public boolean submitSpecial(Object object) {
		final Entity target = (Entity) object;
		getOwner().setNextGraphics(new Graphics(1316));
		getOwner().setNextAnimation(new Animation(7660));
		setNextAnimation(new Animation(8211));
		World.sendProjectile(this, target, 1508, 34, 16, 30, 35, 16, 0);
		WorldTasksManager.schedule(new WorldTask() {

		    @Override
		    public void run() {
			target.applyHit(new Hit(getOwner(), Utils.random(20), HitLook.MAGIC_DAMAGE));
			if (Utils.random(1) == 0)
			    target.getPoison().makePoisoned(60);
			target.setNextGraphics(new Graphics(1511));
		    }
		}, 2);
		return true;
	    }
	}