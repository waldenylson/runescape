package es.thalesalv.runescape.game.npc.combat.impl;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.utils.Utils;

public class AhrimCombat extends CombatScript {

	@Override
	public Object[] getKeys() {
		return new Object[] {2025};
	}

	@Override
	public int attack(NPC npc, Entity target) {
		final NPCCombatDefinitions defs = npc.getCombatDefinitions();
		npc.setNextAnimation(new Animation(defs.getAttackEmote()));
		int damage = getRandomMaxHit(npc, defs.getMaxHit(), NPCCombatDefinitions.MAGE, target);
		if(damage != 0 && target instanceof Player && Utils.random(3) == 0) {	
			target.setNextGraphics(new Graphics(400, 0, 100));
			//Player targetPlayer = (Player) target;
			//int currentLevel = targetPlayer.getSkills().getLevel(Skills.STRENGTH);
			//targetPlayer.getSkills().set(Skills.STRENGTH, currentLevel < 5 ? 0 : currentLevel-5);
		}
		World.sendProjectile(npc, target, defs.getAttackProjectile(), 41, 16, 41, 35, 16, 0);
		npc.setNextGraphics(new Graphics(defs.getAttackGfx()));
		delayHit(npc, 2, target, getMagicHit(npc, damage));
		return defs.getAttackDelay();
	}
}
