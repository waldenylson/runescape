package es.thalesalv.runescape.game.npc.others;

import java.util.ArrayList;
import java.util.List;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.utils.Utils;

@SuppressWarnings("serial")
public class BlackGuardCrossbow extends NPC {

	public BlackGuardCrossbow(int id, WorldTile tile, int mapAreaNameHash, boolean canBeAttackFromOutOfArea, boolean spawned) {
		super(id, tile, mapAreaNameHash, canBeAttackFromOutOfArea, spawned);
		setForceAgressive(true);
	}
	
}
