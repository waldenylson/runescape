package es.thalesalv.runescape.game.npc.combat.impl;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.utils.Utils;

@SuppressWarnings("serial")
public class AirutCombat extends CombatScript {

    @Override
    public Object[] getKeys() {
        return new Object[] { "Airut" };
    }
	
    private static final Animation MELEE = new Animation(22175);
	 
	@Override 
    public int attack(final NPC npc, final Entity target) {
        
        final NPCCombatDefinitions defs = npc.getCombatDefinitions();

        switch (Utils.getRandom(2)) {
	        case 0:
	            npc.setNextAnimation(MELEE);
                delayHit(npc, 1, target, getMeleeHit(npc, getRandomMaxHit(npc, 400, NPCCombatDefinitions.MELEE, target)));
	        break;
	     
		    case 1:
                npc.setNextAnimation(new Animation(22169)); 
                delayHit(npc, 1, target, getMeleeHit(npc, getRandomMaxHit(npc, 400, NPCCombatDefinitions.MELEE, target)));
                npc.setNextForceTalk(new ForceTalk("Rarrgh!!!"));
		    break;
	     
	        case 2:
                npc.setNextAnimation(new Animation(22170));
                delayHit(npc, 1, target, getMeleeHit(npc, getRandomMaxHit(npc, 200, NPCCombatDefinitions.MELEE, target)));
                npc.setNextForceTalk(new ForceTalk("Enough!"));
            break;
	     }
	     return defs.getAttackDelay();
	 
    }
}
	 

