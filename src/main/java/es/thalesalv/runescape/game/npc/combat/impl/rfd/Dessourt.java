package es.thalesalv.runescape.game.npc.combat.impl.rfd;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.npc.combat.CombatScript;
import es.thalesalv.runescape.game.npc.combat.NPCCombatDefinitions;
import es.thalesalv.runescape.utils.Utils;

/**
 * 
 * @author Adam
 * @since Aug, 2nd.
 *
 */

public class Dessourt extends CombatScript{
	@Override
	public Object[] getKeys() {
		
		return new Object[] {3496};
	}

	@Override
	public int attack(NPC npc, Entity target) {
		final NPCCombatDefinitions defs = npc.getCombatDefinitions();
		if (Utils.getRandom(3) == 0) {
	npc.setNextAnimation(new Animation(3508));
		target.applyHit(new Hit(target, Utils.random(160, 170), Hit.HitLook.REGULAR_DAMAGE));;// testiing it has more wa
		npc.setNextGraphics(new Graphics(550));
		npc.setNextForceTalk(new ForceTalk("Hssssssssssss"));
		}
		if (Utils.getRandom(3) == 0) {
			npc.setNextAnimation(new Animation(3508));
			target.applyHit(new Hit(target, Utils.random(160, 170), Hit.HitLook.MAGIC_DAMAGE));// testiing it has more wa
		}
		return defs.getAttackDelay();
}
}
 