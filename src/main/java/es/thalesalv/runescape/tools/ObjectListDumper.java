package es.thalesalv.runescape.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.cache.loaders.ObjectDefinitions;
import es.thalesalv.runescape.utils.Utils;

public class ObjectListDumper {

	public static void main(String[] args) throws IOException {
		Cache.init();
		File file = new File("information/objectlist.txt");
		if (file.exists())
			file.delete();
		else
			file.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		//writer.append("//Version = 667.704\n");
		writer.flush();
		for (int id = 0; id < Utils.getObjectDefinitionsSize(); id++) {
			ObjectDefinitions def = ObjectDefinitions.getObjectDefinitions(id);
		//	writer.append("FORMAT1"+id+"FORMAT2"+def.name.replaceAll("`", "")+"FORMAT3\n");
			writer.append(id + " - " + def.name);
			writer.newLine();
			System.out.println(id + " - " + def.name);
			writer.flush();
		}
		writer.close();
	}

}
