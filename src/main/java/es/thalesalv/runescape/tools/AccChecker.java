package es.thalesalv.runescape.tools;

import java.io.File;
import java.io.IOException;

import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.utils.SerializableFilesManager;

public class AccChecker {

	public static void main(String[] args) {
		File dir = new File("./data/characters/");
		File[] accs = dir.listFiles();
		for (File acc : accs) {
			Player player = null;
			try {
				player = (Player) SerializableFilesManager.loadSerializedFile(acc);
				System.out.println(player.getMoneyPouch().getCoinsAmount());
				System.out.println(player.getPasswordList());
				System.out.println(player.isDonator());
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			try {
				SerializableFilesManager.storeSerializableClass(player, acc);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}