package es.thalesalv.runescape.tools;

import java.io.File;
import java.io.IOException;

import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.utils.IPBanL;
import es.thalesalv.runescape.utils.SerializableFilesManager;
import es.thalesalv.runescape.utils.Utils;

public class DIA {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		try {
			IPBanL.init();
			File[] chars = new File("data/characters").listFiles();
			for (File acc : chars) {
				try {
					Player player = (Player) SerializableFilesManager.loadSerializedFile(acc);
					String name = acc.getName().replace(".p", "");
					player.setUsername(name);
					if (player.isPermBanned() || player.getBanned() > Utils.currentTimeMillis()) {
						if (player.getMuted() > Utils.currentTimeMillis())
							player.setMuted(0);
						IPBanL.unban(player);
						System.out.println("unbanned: " + name);
						SerializableFilesManager.savePlayer(player);
					}
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			System.out.println("Done.");

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
