package es.thalesalv.runescape.tools;

import java.io.IOException;

import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.cache.loaders.NPCDefinitions;
import es.thalesalv.runescape.utils.Utils;

public class NPCCheck {
	
	public static void main(String[] args) throws IOException {
		Cache.init();
		for (int id = 0; id < Utils.getNPCDefinitionsSize(); id++) {
			NPCDefinitions def = NPCDefinitions.getNPCDefinitions(id);
			if (def.name.contains("Elemental")) {
				System.out.println(id+" - "+def.name);
			}
		}
	}

}
