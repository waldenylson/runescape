package es.thalesalv.runescape.tools;

import java.io.File;
import java.io.IOException;

import es.thalesalv.runescape.utils.Utils;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.Skills;
import es.thalesalv.runescape.utils.SerializableFilesManager;

public class EcoReset {

	public static void main(String[] args) throws ClassNotFoundException,
			IOException {		
		File[] chars = new File("data/characters").listFiles();
		for (File acc : chars) {
			try {
				Player player = (Player) SerializableFilesManager
						.loadSerializedFile(acc);
				player.ecoReset();
				SerializableFilesManager.storeSerializableClass(player, acc);
			} catch (Throwable e) {
				e.printStackTrace();
				System.out.println("failed: " + acc.getName());
			}
		}
		System.out.println("Done.");
	}
}