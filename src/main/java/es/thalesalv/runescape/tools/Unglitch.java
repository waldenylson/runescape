package es.thalesalv.runescape.tools;

import java.io.File;
import java.io.IOException;

import es.thalesalv.runescape.Settings;
import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.utils.SerializableFilesManager;
import es.thalesalv.runescape.utils.Utils;


public class Unglitch {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		try {
			Cache.init();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		File dir = new File("./data/characters/");
		File[] accs = dir.listFiles();
		for (File acc : accs) {
			String name = Utils.formatPlayerNameForProtocol(acc.getName().replace(".p", ""));
			System.out.println(acc);
			if (Utils.containsInvalidCharacter(name)) {
				acc.delete();
				return;
			}
			try {
				Player player = (Player) SerializableFilesManager.loadSerializedFile(acc);
					player.setPermBanned(false);
					player.setBanned(0);
					System.out.println(player.getUsername());
				
			} catch( Exception e) {
				e.printStackTrace();
			}
		}
	}

}
