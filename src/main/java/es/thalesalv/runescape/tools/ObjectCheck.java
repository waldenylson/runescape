package es.thalesalv.runescape.tools;

import java.io.IOException;

import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.cache.loaders.ObjectDefinitions;
import es.thalesalv.runescape.utils.Utils;

public class ObjectCheck {

	public static void main(String[] args) throws IOException {
		Cache.init();
		for (int i = 0; i < Utils.getObjectDefinitionsSize(); i++) {
			ObjectDefinitions def = ObjectDefinitions.getObjectDefinitions(i);
			if ( def.containsOption("Steal-from")) {
				System.out.println(def.id+" - "+def.name);
			}
		}
	}

}
