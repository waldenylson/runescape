package es.thalesalv.runescape.util;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;

import org.junit.Test;

public class DatabaseConnectionTest {

	@Test
	public void testDatabaseConnection() throws Exception {
		Connection conn = DatabaseConnection.getConnection();
		assertEquals(Properties.DB_STRING, conn.getMetaData().getURL());
		assertEquals(Properties.DB_USER, conn.getMetaData().getUserName());
	}
}
